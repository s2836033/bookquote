package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    double getBookPrice(String isbn){
        Map<String,Double> booklist = new HashMap<>();
        booklist.put("1",10.0);
        booklist.put("2",45.0);
        booklist.put("3",20.0);
        booklist.put("4",35.0);
        booklist.put("5",50.0);
        for(String val : booklist.keySet()) {
            if (val.equals(isbn)) {
                return booklist.get(val);
            }
        }
        return 0;
    }
}
